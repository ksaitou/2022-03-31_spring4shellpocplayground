
# Spring4ShellのPoCを試すリポジトリ

```bash
# 環境セットアップ
$ pip3 install requests

# Java側のビルド
$ ./gradlew build && cp -f build/libs/handling-form-submission.war ./tomcatjar/handling-form-submission.war

# Dockerの立ち上げ
$ docker compose up

# PoCのPythonの実行
$ python3 exp.py --url http://localhost:18080/handling-form-submission/greeting
# => tomcatjar/ROOT/tomcatwar.jsp が作成される
# => リクエストが飛ぶたびに中身が追記されるはず
```

## Mitigation を試すには

```bash
$ cp -f \
    src/main/java/com/example/handlingformsubmission/_BinderControllerAdvice.java_ \
    src/main/java/com/example/handlingformsubmission/BinderControllerAdvice.java 

$ rm -r tomcatjar/ROOT

# ↑ 再度 ↑ の手順を行って再現しないか確認
```
